from .models import Autor
from libros.serializers import ElementLibroSerializer
from rest_framework import serializers


class AutorSerializer(serializers.HyperlinkedModelSerializer):
    libros = ElementLibroSerializer(many=True, read_only=True)

    class Meta:
        model = Autor
        fields = ['nombre', 'pais', "libros"]
