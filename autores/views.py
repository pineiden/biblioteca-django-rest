from django.shortcuts import render
from rest_framework import viewsets
from .models import Autor
from .serializers import AutorSerializer
# spectacular documentation
from drf_spectacular.utils import extend_schema, OpenApiParameter
from drf_spectacular.types import OpenApiTypes

# Create your views here.

class AutorViewSet(viewsets.ModelViewSet):
    queryset = Autor.objects.all()
    serializer_class = AutorSerializer
