from django.urls import path, include
from rest_framework import routers
from .views import AutorViewSet

app_name='autores'

router = routers.DefaultRouter()
router.register(r'autor', AutorViewSet)

urlpatterns = [
    path('api-rest/', include(router.urls), name='api-rest'),
]
