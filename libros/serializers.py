from .models import Libro
from rest_framework import serializers


class LibroSerializer(serializers.HyperlinkedModelSerializer):
    autor = serializers.StringRelatedField(many=False)
    class Meta:
        model = Libro
        fields = ['titulo', 'tipo', 'resumen', "paginas", "autor"]


class ElementLibroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Libro
        fields = ['titulo', 'tipo', 'resumen', "paginas"]
