from django.db import models
from autores.models import Autor
# Create your models here.


class Libro(models.Model):
    NOVELA='NOVELA'
    CUENTO='CUENTO'
    CRONICA='CRONICA'
    POESIA='POESIA'
    CHOICES=[
        (NOVELA,'novela'),
        (CUENTO,'cuento'),
        (CRONICA,'crónica'),
        (POESIA,'poesía')
    ]
    autor =  models.ForeignKey(Autor,
                               related_name='libros',
                               on_delete=models.SET_NULL,
                               null=True)
    titulo = models.CharField(max_length=300)
    resumen = models.TextField()
    paginas = models.PositiveIntegerField(default=0)
    tipo = models.CharField(max_length=20, default=NOVELA, choices=CHOICES)


    def __str__(self):
        return f"{self.titulo}|{self.autor}"
