from django.urls import path, include
from rest_framework import routers
from .views import LibroViewSet

app_name='libros'

router = routers.DefaultRouter()
router.register(r'libro', LibroViewSet)

urlpatterns = [
    path('api-rest/', include(router.urls), name='api-rest'),
]
