# Generated by Django 3.1.2 on 2020-10-13 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('libros', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='libro',
            name='tipo',
            field=models.CharField(choices=[('NOVELA', 'novela'), ('CUENTO', 'cuento'), ('CRONICA', 'crónica'), ('POESIA', 'poesía')], default='NOVELA', max_length=20),
        ),
    ]
